package gg.zsw.guava;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;

/**
 * 有点意思，存的是KV，但支持反过来用V查K。
 */
public class BiMapGuavaTester {

    public static void main(String args[]) {
        BiMap<Integer, String> empIDNameMap = HashBiMap.create();

        empIDNameMap.put(new Integer(101), "Mahesh");
        empIDNameMap.put(new Integer(102), "Sohan");
        empIDNameMap.put(new Integer(103), "Ramesh");


//        empIDNameMap.put(new Integer(10000), "Mahesh");// 会爆异常
        empIDNameMap.forcePut(new Integer(10000), "Mahesh");// 会覆盖

        //Emp Id of Employee "Mahesh"
        System.out.println(empIDNameMap.inverse().get("Mahesh"));
    }
}//原文出自【易百教程】，商业转载请联系作者获得授权，非商业请保留原文链接：https://www.yiibai.com/guava/guava_bimap.html

