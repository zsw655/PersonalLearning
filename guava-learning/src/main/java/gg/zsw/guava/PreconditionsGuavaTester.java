package gg.zsw.guava;

import com.google.common.base.Preconditions;

public class PreconditionsGuavaTester {

    public static void main(String args[]) {
        PreconditionsGuavaTester guavaTester = new PreconditionsGuavaTester();

//        本来好端端的一个if判断，封装成了异常，还try...catch，代码真多。
        try {
            System.out.println(guavaTester.sqrt(-3.0));
        } catch (IllegalArgumentException e) {
            System.out.println("exception:" + e.getMessage());
        }
        try {
            System.out.println(guavaTester.sum(null, 3));
        } catch (NullPointerException e) {
            System.out.println("exception:" + e.getMessage());
        }
        try {
            System.out.println(guavaTester.getValue(6));
        } catch (IndexOutOfBoundsException e) {
            System.out.println("exception:" + e.getMessage());
        }
    }

    public double sqrt(double input) throws IllegalArgumentException {
        Preconditions.checkArgument(input > 0.0,
                "Illegal Argument passed: Negative value %s.", input);
        return Math.sqrt(input);
    }

    public int sum(Integer a, Integer b) {
        a = Preconditions.checkNotNull(a,
                "Illegal Argument passed: First parameter is Null.");
        b = Preconditions.checkNotNull(b,
                "Illegal Argument passed: Second parameter is Null.");
        return a + b;
    }

    public int getValue(int input) {
        int[] data = {1, 2, 3, 4, 5};
        Preconditions.checkElementIndex(input, data.length,
                "Illegal Argument passed: Invalid index.");
        return 0;
    }
}//原文出自【易百教程】，商业转载请联系作者获得授权，非商业请保留原文链接：https://www.yiibai.com/guava/guava_preconditions_class.html

