package gg.zsw.guava;

import com.google.common.base.CharMatcher;

public class CharMatcherGuavaTester {
    public static void main(String args[]) {
        CharMatcherGuavaTester tester = new CharMatcherGuavaTester();
        tester.testCharMatcher();
    }

    private void testCharMatcher() {

        String mahesh123 = CharMatcher.digit().retainFrom("mahesh123");
        System.out.println(mahesh123); // only the digits


        System.out.println(CharMatcher.whitespace().trimAndCollapseFrom("     Mahesh     Parashar ", ' '));
        // trim whitespace at ends, and replace/collapse whitespace into single spaces
        System.out.println(CharMatcher.javaDigit().replaceFrom("mahesh123", "*")); // star out all digits

        //保留数字或者小写字符
        System.out.println(CharMatcher.javaDigit().or(CharMatcher.javaLowerCase()).retainFrom("MmaheshM123"));
        // eliminate all characters that aren't digits or lowercase
    }
}//原文出自【易百教程】，商业转载请联系作者获得授权，非商业请保留原文链接：https://www.yiibai.com/guava/guava_charmatcher.html

