package gg.zsw.guava;

import com.google.common.collect.ContiguousSet;
import com.google.common.collect.DiscreteDomain;
import com.google.common.collect.Range;
import com.google.common.primitives.Ints;

/**
 * 线段匹配方面的神器哟
 */
public class RangeGuavaTester {

    public static void main(String args[]) {
        RangeGuavaTester tester = new RangeGuavaTester();
//        tester.testRange();
        tester.testRangeDouble();
    }

    /**
     * 浮点也是阔以哒！！
     */
    private void testRangeDouble() {
        Range<Double> range1 = Range.closed(0.0, 9.0);
        Range<Double> range2 = Range.closed(0.0, 10.0);
        boolean contains = range1.contains(4.5);
        System.out.println(contains);

        boolean encloses = range1.encloses(range2);
        System.out.println(encloses);
    }


    private void testRange() {

        //create a range [a,b] = { x | a <= x <= b}  闭区间
        Range<Integer> range1 = Range.closed(0, 9);
        System.out.print("[0,9] : ");
        printRange(range1);
        System.out.println("5 is present: " + range1.contains(5));
        System.out.println("(1,2,3) is present: " + range1.containsAll(Ints.asList(2, 1, 3)));// 顺序无关的
        System.out.println("Lower Bound: " + range1.lowerEndpoint());
        System.out.println("Upper Bound: " + range1.upperEndpoint());

        //create a range (a,b) = { x | a < x < b} 开区间
        Range<Integer> range2 = Range.open(0, 9);
        System.out.print("(0,9) : ");
        printRange(range2);

        // 开闭、闭开也是可以的哟
        //create a range (a,b] = { x | a < x <= b}
        Range<Integer> range3 = Range.openClosed(0, 9);
        System.out.print("(0,9] : ");
        printRange(range3);

        //create a range [a,b) = { x | a <= x < b}
        Range<Integer> range4 = Range.closedOpen(0, 9);
        System.out.print("[0,9) : ");
        printRange(range4);

        //
        //create an open ended range (9, infinity
        Range<Integer> range5 = Range.greaterThan(9);
        System.out.println("(9,infinity) : ");
        System.out.println("Lower Bound: " + range5.lowerEndpoint());
        System.out.println("Upper Bound present: " + range5.hasUpperBound());

        Range<Integer> range6 = Range.closed(3, 5);
        printRange(range6);

        //check a subrange [3,5] in [0,9]
        System.out.println("[0,9] encloses [3,5]:" + range1.encloses(range6));

        Range<Integer> range7 = Range.closed(8, 20);
        printRange(range7);
        //check ranges to be connected
        System.out.println("[0,9] is connected [8,20]:" + range1.isConnected(range7));// 可以合并成1段区间

        Range<Integer> range8 = Range.closed(5, 15);

        //intersection
        printRange(range1.intersection(range8));

        //span
        printRange(range1.span(range8));
    }

    private void printRange(Range<Integer> range) {
        System.out.print("[ ");
        for (int grade : ContiguousSet.create(range, DiscreteDomain.integers())) {
            System.out.print(grade + " ");
        }
        System.out.println("]");
    }
}//原文出自【易百教程】，商业转载请联系作者获得授权，非商业请保留原文链接：https://www.yiibai.com/guava/guava_range_class.html

