package gg.zsw.guava;

import com.google.common.base.Splitter;

public class SplitterGuavaTester {
    public static void main(String args[]) {
        SplitterGuavaTester tester = new SplitterGuavaTester();
        tester.testSplitter();
    }

    private void testSplitter() {
        System.out.println(Splitter.on(',')//按逗号切分字符串
                .trimResults()//首尾无空格
                .omitEmptyStrings()// 跳过空字符串
                .split("the ,quick, , brown         , fox,              jumps, over, the, lazy, little dog."));
    }
}//原文出自【易百教程】，商业转载请联系作者获得授权，非商业请保留原文链接：https://www.yiibai.com/guava/guava_spliter.html

