package gg.zsw.guava;

import java.util.Arrays;

import com.google.common.base.Joiner;

/**
 * 字符串处理，不明白深层的含义
 */
public class JoinerGuavaTester {
    public static void main(String args[]) {
        JoinerGuavaTester tester = new JoinerGuavaTester();
        tester.testJoiner();
    }

    private void testJoiner() {
        String s = Joiner.on(",")
                .skipNulls()
                .join(Arrays.asList(1, 2, 3, 4, 5, null, 6));
        System.out.println(s);


    }
}//原文出自【易百教程】，商业转载请联系作者获得授权，非商业请保留原文链接：https://www.yiibai.com/guava/guava_joiner.html

