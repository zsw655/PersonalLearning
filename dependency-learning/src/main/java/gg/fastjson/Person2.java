package gg.fastjson;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.util.Date;

@Data
public class Person2 {

    /**
     * 造json时，不序列化
     */
    @JSONField(name="AGE", serialize=false)
    private int age;

    /**
     * ordinal指定顺序
     */
    @JSONField(name="LAST NAME", ordinal = 2)
    private String lastName;

    @JSONField(name="FIRST NAME", ordinal = 1)
    private String firstName;

    @JSONField(name="DATE OF BIRTH", format="dd/MM/yyyy", ordinal = 3)
    private Date dateOfBirth;

    public Person2(int age, String firstName,String lastName,  Date dateOfBirth) {
        super();
        this.age = age;
        this.firstName= firstName;
        this.lastName= lastName;
        this.dateOfBirth = dateOfBirth;
    }

    // 标准 getters & setters
}