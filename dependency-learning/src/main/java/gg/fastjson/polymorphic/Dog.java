package gg.fastjson.polymorphic;

import com.alibaba.fastjson.annotation.JSONType;

/**
 * @author zhou.sw
 * @Date 2020/7/3 11:19
 */
@JSONType(typeName = "dog")
public class Dog extends Animal{
    public String dogName;
}
