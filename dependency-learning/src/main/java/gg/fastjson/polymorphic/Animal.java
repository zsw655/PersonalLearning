package gg.fastjson.polymorphic;

import com.alibaba.fastjson.annotation.JSONType;

/**
 * @author zhou.sw
 * @Date 2020/7/3 11:19
 */
@JSONType(seeAlso = {Dog.class, Cat.class})
public class Animal {
}
