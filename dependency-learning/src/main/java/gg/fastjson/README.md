# 源码
https://github.com/alibaba/fastjson
# 学习地址
https://www.runoob.com/w3cnote/fastjson-intro.html

# maven
```xml
<dependency>
    <groupId>com.alibaba</groupId>
    <artifactId>fastjson</artifactId>
    <version>1.2.62</version>
</dependency>
```
# 感悟
学几个基本使用就行了，其他高级功能看看就好，要用的时候再查




