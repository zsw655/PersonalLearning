package gg.fastjson.polymorphic;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author zhou.sw
 * @Date 2020/7/3 11:21
 */
public class AnimalTest {

    @Test
    public void test1() {
        Dog dog = new Dog();
        dog.dogName = "dog1001";

        String text = JSON.toJSONString(dog, SerializerFeature.WriteClassName);
        System.out.println(text);
        Assert.assertEquals("{\"@type\":\"dog\",\"dogName\":\"dog1001\"}", text);

        Animal dog2 = JSON.parseObject(text, Animal.class);
        if(dog2 instanceof Dog){
            System.out.println(1212);
        }

    }


    @Test
    public void test2() {
        Cat cat = new Cat();
        cat.catName = "cat2001";

        String text = JSON.toJSONString(cat, SerializerFeature.WriteClassName);
        Assert.assertEquals("{\"@type\":\"cat\",\"catName\":\"cat2001\"}", text);

        Cat cat2 = (Cat) JSON.parseObject(text, Animal.class);

        Assert.assertEquals(cat.catName, cat2.catName);
    }


}