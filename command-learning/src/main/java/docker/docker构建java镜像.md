# DNS配置8.8.8.8（也可以pom.xml中配置阿里云镜像仓库）
不配置的话，可能报如下错
Error response from daemon: Get https://registry-1.docker.io/v2/: dial tcp: lookup registry-1.docke



# pom.xml同级目录下创建Dockerfile
```
FROM openjdk:8-jdk-alpine
# 把container中的/tmp挂载到外部一个匿名volume
VOLUME /tmp

#Providing label to our container so that it can be used to stop the container instead of using containerID
LABEL appname=what-the-fuck

#Copying the jar file to app.jar
COPY ./target/vrs-1.0.jar app.jar

RUN echo "Asia/Shanghai" > /etc/timezone

RUN sh -c 'touch /app.jar'

#The container launched using this image will be running jar file on specified port.
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]
```
主要是设时区、容器名称、时区、修改包时间、启动命令
