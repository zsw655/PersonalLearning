# detection.sh

```shell script
#启动方法：nohup sh [脚本名] [jar包名] >/dev/null 2>&1 &
log_dir=/Log/user/monitor/
#------------------------------CPU 和内存 变化记录到文件中-----------------------------
function detection(){

    pid=`ps -ef|grep $1 |grep java|grep -v 'grep' |grep -v $0 |awk '{print $2}'`
    var=`date "+%Y-%m-%d %H:%M:%S"`
    var1=`date "+%Y-%m-%d"`
    if test $# -eq 0
     then
        exit 1
     elif  [[ "$pid" != "" ]]
        then
        top -p $pid -b -d 1 -n 1| awk '{print strftime("%Y-%m-%d %H:%M:%S"),$0;}'>> ${log_dir}/cpu.log.$var1
        echo '' >>${log_dir}/cpu.log.$var1
     else
       echo "获取不到内存信息，请检查jar是否在运行！" 
     fi
}

while true 
do   
   detection $1
   sleep 1
done
```