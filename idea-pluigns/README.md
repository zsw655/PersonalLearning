# 说明
idea有非常多好用的插件，感受一下现代化编码的威力吧。
一些idea配置也会记录到这里面来。

# 📕 编码规范提速类插件

## Key promoter X
快捷键简直要命。帮你记忆提升工作效率。
Meta键是什么来着？

## Lombok
什么，不识Lombok？装尽plugin也枉然

## Alibaba Java Coding Guidelines
阿里巴巴Java代码规范插件。渣渣代码直线下降。

## FindBugs
FindBugs是在Java程序找到bug的一种静态分析工具。
它通过检查类或者JAR文件，将字节码与一组缺陷模式进行对比以发现可能的问题。
有了静态分析工具，就可以在不实际运行程序的情况对软件进行分析，发现一些人工很难发现的潜在隐患问题。

## codota
智能代码提示、API的示例代码
都是某些开源代码来的，学习价值极高！要用github账号登陆的。

![codota插件安装成功提示](./src/main/java/codota/doc/codota插件.png)

## Java Stream Debugger
Stream操作好用，但很难去理解。有了这个插件，就好办多了。

## GsonFormat
自动生成json对象哟

## GenerateAllSetter
alt+enter 自动把类所有的set方法搞出来，操作POJO类时非常需要哟。

## JUnitGenerator V2.0
什么？有插件，不用手动又建包又建Test类了

## Auto filling Java call arguments
Alt+Enter 组合键，调出 "Auto fill call parameters" 自动使用该函数定义的参数名填充。

## String Manipulation
强大的字符串转换工具。使用快捷键，Alt+m。

# 📕 读码分析类插件

## Rainbow Brackets
彩虹括号见过没？立马就能看出括号哪里到哪里。

## SequenceDiagram
源码阅读时，直接根据代码调用链路自动生成时序图。
再也不用跳转来跳转去的了。

## Grep Console
黑白日志简直不能看。彩色log见过没？

## IdeaJad
反编译工具，那些jd-gui什么的就不用了


# 📕 特定领域类插件

## Maven Helper
从此不用头疼jar包冲突啦，打开 pom.xml 文件时，点击 "Dependency Analyzer" ，排除冲突项。<br>
什么？没找到Dependency Analyzer？pom.xml下面位置，百度一下就知道了。


## Database
mybatis生成的时候用到。不然用navicat就够啦。

## MyBatisCodeHelperPro
https://gejun123456.github.io/MyBatisCodeHelper-Pro/#/  
先找找破解版试用一下，感觉非常非常好用，就用它了！
不算商用的话还是支持一下作者吧，也不贵。
其他mybatis插件可以不用了，还有这些插件，有兴趣可以去试试。

Free Mybatis plugin

Mybatis-log-plugin

Free-idea-mybatis

## Zookeeper
新版idea用不了。唉...

## VisualVM Launcher
idea自带就是好

## Jrebel
热部署插件，一脸懵逼中。安装和激活方式：
https://blog.csdn.net/lianghecai52171314/article/details/105637251

可以提高开发效率，注意是提高开发效率。
暂时不清楚正在运行的项目能不能用这种方式来实现无缝升级？要是可以的话，那多爽啊。
类似的方式还有spring-boot-devtools


## RestfulToolkit
鬼知道你那URL完整地址是个啥，有这个工具就好搞了！
自从升级了最新版的idea，这工具就用不了了，唉。。。



# 📕 装逼类插件
## power-mode
还有谁？妈的还有谁？
要比较新版本的idea才支持。

## WakaTime
想查看自己都在做啥子，在哪一个项目中花的时间最多，该插件会给我们提供一个很好的UI界面向我们展示。
免费版可以显示最近14天的情况。要到官网去看。和gitlab关联，没和gitee关联。
https://wakatime.com


## Statistic
统计写了n行代码，项目有多少代码，某目录下有多少行代码

## Gitmoji
git表情包，屌屌的


## Background Image Plus
背景图片哟

## Material Theme UI 
主题哟。
本人只是想改改颜色而已。字体图标那些就不选了。

## Translation
比你开个窗口专门做翻译要强。

## CodeGlance
代码缩略图

个人挺不习惯的，就算看到缩略图也找不到想找的代码呀！

## JOL Java Object Layout
看一下一个java对象到底占用多少内存空间。大概是吃撑了，不过看看变量还蛮舒服的。

## Stack Overflow
什么，搜索有毒，找半天解决不了？试试插件搜索吧

## CMD support
运行.cmd文件需要插件。好像直接点开文件夹双击运行也很方便啊，要这插件干嘛叻。


# 📕 文艺类插件
怎么能只知道写代码呢？

## Markdown Navigator Enhanced
写markdown用的，看介绍是非常好用的，不过收费，还是算了。

安装完后，弹出选择

![啊](./src/main/java/MarkdownNavigator/doc/选择markdown工具.png)

试试在线markdown吧，小书匠看起来就不错：
http://markdown.xiaoshujiang.com/


## plantUML
画UML图的
### 安装
注意这个配置，其他内容网上搜
![a](./src/main/java/plantUML/doc/idea配置.png)

## IDEA Mind Map
思维导图

## Pomodoro-tm
番茄工作法，安装完，底下就有个时钟一样的图标。点击就能计时。
默认25分钟就行，别改来改去的，多麻烦啊。