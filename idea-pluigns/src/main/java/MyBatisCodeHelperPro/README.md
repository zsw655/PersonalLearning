# 加入依赖
```
    <dependencies>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-context</artifactId>
            <version>5.2.5.RELEASE</version>
            <scope>compile</scope>
        </dependency>
        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <version>1.18.12</version>
        </dependency>
        <dependency>
            <groupId>com.baomidou</groupId>
            <artifactId>mybatis-plus</artifactId>
            <version>3.1.0</version>
        </dependency>
    </dependencies>
```
# 工程结构
```
project
   ├-------createTable
   ├-------dao
   |        ├--------PO
   |        └--------mapper
   └-------service
               └--------model
```
# 说明
似乎这个工程创建不出test，没关系，mybatis的学习在个人的web项目中进行体现。




