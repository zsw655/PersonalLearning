package MyBatisCodeHelperPro.dao.DO;

import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;

/**
 *  
 * @author zhou.sw
 * @Date 2020/6/23 12:14
 */
/**
    * user
    */
@Data
public class User {
    /**
    * userId
    */
    private Integer userId;

    /**
    * userName
    */
    private String userName;

    /**
    * birth
    */
    private Date birth;

    /**
    * 工资
    */
    private BigDecimal salary;
}