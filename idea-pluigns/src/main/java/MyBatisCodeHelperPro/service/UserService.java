package MyBatisCodeHelperPro.service;

import MyBatisCodeHelperPro.dao.DO.User;
    /**
 *  
 * @author zhou.sw
 * @Date 2020/6/23 12:14
 */
public interface UserService{


    int deleteByPrimaryKey(Integer userId);

    int insert(User record);

    int insertSelective(User record);

    User selectByPrimaryKey(Integer userId);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);

}
