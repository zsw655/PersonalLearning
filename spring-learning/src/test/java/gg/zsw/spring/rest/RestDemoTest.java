package gg.zsw.spring.rest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

/**
 * @author zhou.sw
 * @Date 2020/7/4 14:06
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = gg.zsw.spring.rest.Application.class)
//@ComponentScan("gg.zsw.spring.rest")
public class RestDemoTest {

    @Autowired
    private RestTemplate restTemplate;

    @Test
    public void test1() {
        Map<String, String> map = new HashMap<>();
        map.put("cType", "0");

        String url = "http://192.168.7.242:8367/cors/openCorsData/getcorsEncryptedMsg";

        DemoClass demoClass = restTemplate.getForObject(url, DemoClass.class, map);
        System.out.println(demoClass.getCode());
        System.out.println(demoClass.getMessage());
    }

}