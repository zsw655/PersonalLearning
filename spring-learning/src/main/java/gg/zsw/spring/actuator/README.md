# spring-boot-starter-actuator
实时或定期监控服务的可用性。
Spring Boot的actuator（健康监控）功能提供了很多监控所需的接口，可以对应用系统进行配置查看、相关功能统计等。

# 动态刷新配置
参考地址：
https://www.jianshu.com/p/230af40377cf
