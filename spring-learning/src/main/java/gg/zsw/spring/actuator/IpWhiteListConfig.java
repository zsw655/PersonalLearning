package gg.zsw.spring.actuator;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;


/**
 * @author zhou.sw
 * @Date 2020/7/23 10:55
 */
@ConfigurationProperties(prefix = "access-contrl")
// 这个注解弄不出来？？
//@RefreshScope
@Component
public class IpWhiteListConfig {


    private boolean enable;

    private Set<String> whiteList;

    public IpWhiteListConfig() {
        this.whiteList = new HashSet<>();
    }

    public Set<String> getWhiteList() {
        return whiteList;
    }

    public void setWhiteList(Set<String> whiteList) {
        this.whiteList = whiteList;
    }

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }
}