package gg.zsw.spring.rest;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * @author zhou.sw
 * @Date 2020/7/4 14:16
 */
@Data
public class DemoClass {
    @JSONField(name = "code")
    private int code;

    @JSONField(name = "message")
    private String message;

//    @JSONField(name = "result")
//    private String result;
}
