package gg.zsw.java.thread;

/**
 * 常用做法：强行把单例变成多例。单例里面只要搞一下ThreadLocal就能达到多例一样的并行效果。
 *
 * @author zhou.sw
 * @Date 2020/7/23 15:35
 */
public class ThreadLocalDemo {

    /**
     * 注意: 是lambda创建
     */
    private static ThreadLocal<Object> threadLocal = ThreadLocal.withInitial(ThreadLocalDemo::create);

    private static Object create() {
        Object result = null;

        return result;
    }

    // ③直接返回线程本地变量
    public static Object getObject() {
        return threadLocal.get();
    }
}
