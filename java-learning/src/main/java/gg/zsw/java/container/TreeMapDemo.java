package gg.zsw.java.container;

import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 * 有序的。
 *
 * @author zhou.sw
 * @Date 2020/7/29 10:56
 */
public class TreeMapDemo {
    public static void main(String[] args) {
        TreeMap<Integer, String> sPara = new TreeMap<>();
        sPara.put(2, "abc");
        sPara.put(123, "cds");
        sPara.put(1231, "ads");
        sPara.put(43, "fff");
        sPara.put(352, "ccc");
        sPara.put(2523, "ddd");
        sPara.put(235435, "qqqqq");
        sPara.put(234523, "knns");
        sPara.put(5474, "ioio");
        Set<Integer> keys = sPara.keySet();
//        for (Integer key : keys){
//            System.out.println(key);
//        }
        for (Map.Entry<Integer, String> entry : sPara.entrySet()) {
            int key = entry.getKey();
            String msg = entry.getValue();
            System.out.println(key);
        }
    }
}
