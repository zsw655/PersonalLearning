package gg.zsw;

/**
 * @author zhou.sw
 * @Date 2021/3/17 13:49
 */
public class optimize {

    public void fun1(int cond) {
        if (cond == 0) {
            for (int j = 0; j < 5; j++) {
                System.out.println("" + j);
            }
        } else {
            System.out.println("" + cond);
        }
    }

    public void fun2(int cond) {
        int end = 0;
        if (cond == 0) {
            end = 5;
        } else {
            end = cond + 1;
        }

        for (int j = cond; j < end; j++) {
            System.out.println(j);
        }
    }

    public static void main(String[] args) {
        new optimize().fun1(0);
        new optimize().fun1(1);
        new optimize().fun2(0);
        new optimize().fun2(1);
    }
}
